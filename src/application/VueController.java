package application;

import javafx.fxml.FXML;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;

public class VueController {

	@FXML
	private ImageView usa;
	
	@FXML
	private ImageView eu;
	
	@FXML
	private ImageView ca;
	
	@FXML
	private ImageView imgCourante;
	
	public void changerDrapeau(MouseEvent e) {
		String imgClique = e.getSource().toString();
		String srcImg = imgClique.substring(imgClique.indexOf("id=") + 3, imgClique.indexOf(",")) + ".png";
		System.out.println(srcImg);
		Image newImage = new Image(srcImg);
		imgCourante.setImage(newImage);
	}
	
}
